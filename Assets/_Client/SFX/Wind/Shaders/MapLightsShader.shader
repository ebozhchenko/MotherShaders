Shader "Unlit/MapLightsShader"
{
    Properties
    {
        _LightColor("Light Color", Color) = (1, 1, 1, 1)
        _ShadowColor("Shadow Color", Color) = (1, 1, 1, 1)

        _Scale("Scale", float) = 10
        _Speed("Speed", float) = 1
        _Step("Step", float) = 0.5
        _Transition("Transition", float) = 0.1
    }
    SubShader
    {
        Tags {
            "RenderType" = "Transparent"
            "Queue" = "Transparent"
        }

        Blend SrcAlpha OneMinusSrcAlpha

        ZWrite off
        Cull off
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "UnityCG.glslinc"

            #include "ClassicNoise3D.hlsl"


            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float3 worldPos : TEXCOORD1;
            };


            float4 _LightColor;
            float4 _ShadowColor;

            float _Scale;
            float _Speed;
            float _Step;
            float _Transition;


            v2f vert(appdata v)
            {
                v2f o;

                o.vertex = UnityObjectToClipPos(v.vertex);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);

                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float noise_1 = (cnoise(float3(i.worldPos.xy, _Speed * _Time.y) / _Scale) + 1) / 2;
                float noise_2 = (cnoise(float3(i.worldPos.xy, -_Speed * _Time.y) / _Scale) + 1) / 2;

                float noise = noise_1 * noise_2;

                float4 light = _ShadowColor;
                if (noise > _Step)
                    light = _LightColor;

                if (abs(noise - _Step) < _Transition / 2)
                    light = lerp(_ShadowColor, _LightColor, (noise - (_Step - _Transition / 2)) / _Transition);

                return light;
            }
            ENDCG
        }
    }
}
