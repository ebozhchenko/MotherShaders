Shader "Unlit/FogShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _NoiseSpeed("Speed", Vector) = (0, 0, 1, 0)
        _NoiseScale("Scale", float) = 1
        _NoiseStrength("Strength", float) = 0.5
        _FirstStep("First Step", float) = 0.5
        _FirstColor("First Color", Color) = (1, 1, 1, 1)
        _SecondStep("Second Step", float) = 0.7
        _SecondColor("Second Color", Color) = (1, 1, 1, 1)

        _Octaves("Octaves", float) = 2
        _Lacunarity("Lacunarity", float) = 2
        _Persistance("Persistance", float) = 0.5
    }
    SubShader
    {
        Tags {
            "RenderType" = "Transparent"
            "Queue" = "Transparent"
        }

        Blend SrcAlpha OneMinusSrcAlpha

        ZWrite off
        Cull off
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "UnityCG.glslinc"

            #include "ClassicNoise3D.hlsl"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 worldPos : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            float _NoiseScale;
            float3 _NoiseSpeed;
            float _NoiseStrength;
            float _FirstStep;
            float _SecondStep;

            float4 _FirstColor;
            float4 _SecondColor;

            float _Octaves;
            float _Lacunarity;
            float _Persistance;


            v2f vert (appdata v)
            {
                v2f o;

                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);

                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float noise_value = 0;
                float octave = 0;
                float max_value = 0;
                while (octave < _Octaves)
                {
                    float fluence = pow(_Persistance, octave);
                    noise_value += fluence * (cnoise((float3(i.worldPos.xy, 0) + _NoiseSpeed * _Time.y) / (_NoiseScale * pow(_Lacunarity, octave))) + 1) / 2;

                    max_value += fluence;
                    octave++;
                }
                noise_value /= max_value;

                fixed4 fog_map = tex2D(_MainTex, i.uv);
                float fog = noise_value * _NoiseStrength + fog_map;

                float4 result = float4(0, 0, 0, 0);
                if (fog > _SecondStep)
                    result = _SecondColor;
                else if (fog > _FirstStep)
                    result = _FirstColor;

                return result;
            }
            ENDCG
        }
    }
}
