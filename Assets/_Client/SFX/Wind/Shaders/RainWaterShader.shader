Shader "Unlit/RainWaterShader"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
        _DropTex("Drop Texture", 2D) = "black" {}
        _WaveColor("Wave Color", Color) = (1, 1, 1, 1)
        _WaterColor("Water Color", Color) = (1, 1, 1, 1)
        _DropSize("Drop Size", Vector) = (1, 1, 1, 0)
        _DropsFrames("Drops Frames", float) = 4
        _DropFps("Drop FPS", float) = 4
        _DropStep("Drop Step", float) = 0.5
        _Speed("Speed", float) = 1
        _MapFluence("Map Fluence", float) = 1
    }
    SubShader
    {
        Tags {
            "RenderType" = "Transparent"
            "Queue" = "Transparent"
        }

        Blend SrcAlpha OneMinusSrcAlpha

        ZWrite off
        Cull off
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "UnityCG.glslinc"

            #include "ClassicNoise3D.hlsl"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 worldPos : TEXCOORD1;
            };


            sampler2D _MainTex;
            float4 _MainTex_ST;
            sampler2D _DropTex;
            float4 _DropTex_ST;

            float4 _WaveColor;
            float4 _WaterColor;

            float3 _DropSize;
            float _DropFps;
            float _DropsFrames;
            float _DropStep;
            float _Speed;
            float _MapFluence;


            float random(float2 uv)
            {
                return frac(sin(dot(uv, float2(12.9898, 78.233))) * 43758.5453123);
            }

            float4 get_drops(float2 drop_size, float3 world_pos)
            {
                float frame = floor((_Time.y * _DropFps) % _DropsFrames);
                float2 world_uv = float2((abs(world_pos.x) % drop_size.x) / drop_size.x, (abs(world_pos.y) % drop_size.y) / drop_size.y);
                float2 frame_uv = float2((world_uv.x + frame) / _DropsFrames, world_uv.y);

                return tex2D(_DropTex, frame_uv);
            }

            float get_alpha(float2 drop_size, float3 world_pos)
            {
                float2 drop_position = float2(floor(world_pos.x / drop_size.x), floor(world_pos.y / drop_size.y));
                float rand_id = random(drop_position);
                //float drop_alpha = (cnoise(float3(drop_position * 40.21, (rand_id + _Time.y) * _Speed)) + 1) / 2;
                float drop_alpha = ((_Time.y * _Speed) + rand_id) % 1;
                return drop_alpha > _DropStep ? drop_alpha : 0;
            }

            float get_drops_result(float2 drop_size, float3 world_pos)
            {
                float4 drops = get_drops(drop_size, world_pos);
                float alpha = get_alpha(drop_size, world_pos);
                return drops.a * alpha;
            }


            v2f vert(appdata v)
            {
                v2f o;

                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _DropTex);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);

                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float drops_result1 = get_drops_result(_DropSize.xy * _DropSize.z, i.worldPos);
                float drops_result2 = get_drops_result(_DropSize.xy * _DropSize.z, i.worldPos * 1.9);
                float drops_result3 = get_drops_result(_DropSize.xy * _DropSize.z, i.worldPos * 1.5);

                float drops = max(drops_result1, drops_result2);
                drops = max(drops, drops_result3);

                fixed4 water_map = tex2D(_MainTex, i.uv);

                return float4(_WaveColor.rgb, _WaveColor.a * drops * water_map.a);
            }
            ENDCG
        }
    }
}
