Shader "Unlit/RainPlaneShader"
{
    Properties
    {
        _DropsColor("Drops color", Color) = (1, 1, 1, 1)
        _LinesDensity("Lines density", float) = 10
        _LinesThickness("Lines thickness", float) = 10
        _LinesDirection("Lines direction", Vector) = (1, 0, 0, 0)
        _LinesSpeed("Lines speed", Vector) = (1, 0, 1, 0)
        _NoiseScale("Drops scale", Vector) = (1, 1, 1, 0)
        _NoiseDirection("Drops direction", Vector) = (1, 0, 0, 0)
        _NoiseSpeed("Drops speed", Vector) = (1, 0, 1, 0)
        _NoiseStep("Drops thickness", float) = 10
    }
        SubShader
        {
            Tags {
                "RenderType" = "Transparent"
                "Queue" = "Transparent"
            }

            Blend SrcAlpha OneMinusSrcAlpha

            ZWrite off
            Cull off
            LOD 100

            Pass
            {
                CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag

                #include "UnityCG.cginc"
                #include "UnityCG.glslinc"

                #include "ClassicNoise3D.hlsl"

                struct appdata
                {
                    float4 vertex : POSITION;
                    float2 uv : TEXCOORD0;
                };

                struct v2f
                {
                    float2 uv : TEXCOORD0;
                    float4 vertex : SV_POSITION;
                    float3 worldPos : TEXCOORD1;
                };


                float4 _DropsColor;

                float _LinesDensity;
                float _LinesThickness;
                float2 _LinesDirection;
                float3 _LinesSpeed;

                float3 _NoiseScale;
                float2 _NoiseDirection;
                float4 _NoiseSpeed;
                float _NoiseStep;


                v2f vert(appdata v)
                {
                    v2f o;

                    o.vertex = UnityObjectToClipPos(v.vertex);
                    o.uv = v.uv;
                    o.worldPos = mul(unity_ObjectToWorld, v.vertex);

                    return o;
                }

                fixed4 frag(v2f i) : SV_Target
                {
                    float k = 2 * UNITY_PI / _LinesDensity;
                    float c = sqrt(9.8 / k);
                    float2 d = normalize(_LinesDirection);
                    float f = k * (dot(d, i.worldPos.xy - (c * _Time.y * _LinesSpeed.z) * _LinesSpeed.xy));
                    float lines = (sin(f) + 1) / 2;
                    float lines_step = 1 - 1 / _LinesThickness;
                    lines = lines > lines_step ? 1 : 0;

                    float2 scaled_position = float2(i.worldPos.x / _NoiseScale.x, i.worldPos.y / _NoiseScale.y) / _NoiseScale.z;
                    float2 noise_position = scaled_position - (_Time.y * _NoiseSpeed.w) * _NoiseSpeed.xy;
                    float noise = (cnoise(float3(noise_position.xy, -_Time.y * _NoiseSpeed.w * _NoiseSpeed.z)) + 1) / 2;
                    float noise_step = 1 - 1 / _NoiseStep;
                    noise = noise > noise_step ? 1 : 0;

                    float drops = noise * lines;

                    return drops * _DropsColor;
                }
                ENDCG
            }
        }
}
