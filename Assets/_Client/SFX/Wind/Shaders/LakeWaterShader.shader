Shader "Unlit/LakeWaterShader"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
        _NoiseSpeed("Noise Speed", Vector) = (0, 0, 1, 0)
        _NoiseScale("Noise Scale", float) = 1
        _MaskScale("Mask Scale", float) = 1
        _MaskSpeed("Mask Speed", float) = 0
        _Step("Step", float) = 0.5
        _WaveColor("Wave Color", Color) = (1, 1, 1, 1)
        _WaterColor("Water Color", Color) = (1, 1, 1, 1)
        _Wavelength("Wave Length", Float) = 10
        _WaveSpeed("Wave Speed", Vector) = (0, 0, 1, 0)
        _WaveEdge("Wave Edge", Float) = 10
    }
        SubShader
        {
            Tags {
                "RenderType" = "Transparent"
                "Queue" = "Transparent"
            }

            Blend SrcAlpha OneMinusSrcAlpha

            ZWrite off
            Cull off
            LOD 100

            Pass
            {
                CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag

                #include "UnityCG.cginc"
                #include "UnityCG.glslinc"

                #include "ClassicNoise3D.hlsl"

                struct appdata
                {
                    float4 vertex : POSITION;
                    float2 uv : TEXCOORD0;
                };

                struct v2f
                {
                    float2 uv : TEXCOORD0;
                    float4 vertex : SV_POSITION;
                    float3 worldPos : TEXCOORD1;
                };

                sampler2D _MainTex;
                float4 _MainTex_ST;

                float _NoiseScale;
                float3 _NoiseSpeed;
                float _Step;

                float _MaskScale;
                float _MaskSpeed;

                float4 _WaveColor;
                float4 _WaterColor;

                float _Wavelength;
                float3 _WaveSpeed;
                float _WaveEdge;


                v2f vert(appdata v)
                {
                    v2f o;

                    o.vertex = UnityObjectToClipPos(v.vertex);
                    o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                    o.worldPos = mul(unity_ObjectToWorld, v.vertex);

                    return o;
                }

                fixed4 frag(v2f i) : SV_Target
                {
                    float k = 2 * UNITY_PI / _Wavelength;
                    float c = sqrt(9.8 / k);
                    float2 d = normalize(_WaveSpeed);
                    float f = k * (dot(d, i.worldPos.xy) - c * _Time.y);
                    float wave = sin(f);

                    float noise_value = (cnoise((float3(i.worldPos.xy, 0) + _NoiseSpeed * _Time.y) / _NoiseScale) + 1) / 2;
                    float noise_mask_value = (cnoise((float3(i.worldPos.xy, 0) + float3(_NoiseSpeed.xy, _MaskSpeed) * _Time.y) / _MaskScale) + 1) / 2;

                    float wave_value = noise_value * noise_mask_value * (wave - _WaveEdge);
                    fixed4 water_map = tex2D(_MainTex, i.uv);

                    float water = wave_value * water_map.r * water_map.a;

                    return water > _Step ? _WaveColor : _WaterColor;
                }
                ENDCG
            }
        }
}
