Shader "Unlit/FireMonsterShader"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
        _FireMap("Texture", 2D) = "black" {}
        _ColorMap("Texture", 2D) = "white" {}

        _NoiseScale("Noise Scale", Vector) = (1, 1, 1, 0)
        _NoiseSpeed("Noise Speed", Vector) = (0, 0, 0, 0)
        _NoiseStep("Noise Step", float) = 0.5
        _NoiseStrength("Noise Strength", float) = 0.5

        _CutScale("Cut Scale", Vector) = (1, 1, 1, 0)
        _CutSpeed("Cut Speed", Vector) = (0, 0, 0, 0)
        _CutStep("Cut Step", float) = 0.5
    }
        SubShader
    {
        Tags {
            "RenderType" = "Transparent"
            "Queue" = "Transparent"
        }

        Blend SrcAlpha OneMinusSrcAlpha

        ZWrite off
        Cull off
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "UnityCG.glslinc"

            #include "ClassicNoise3D.hlsl"


            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                float3 worldPos : TEXCOORD1;
            };


            sampler2D _MainTex;
            float4 _MainTex_ST;
            sampler2D _FireMap;
            float4 _FireMap_ST;
            sampler2D _ColorMap;
            float4 _ColorMap_ST;

            float3 _NoiseScale;
            float3 _NoiseSpeed;
            float _NoiseStep;
            float _NoiseStrength;

            float3 _CutScale;
            float3 _CutSpeed;
            float _CutStep;


            v2f vert(appdata v)
            {
                v2f o;

                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);

                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                fixed4 tex = tex2D(_MainTex, i.uv);
                fixed4 color = tex2D(_ColorMap, i.uv);
                float map = tex2D(_FireMap, i.uv).a;

                float3 noise_position = (float3(i.worldPos.x / _NoiseScale.x, i.worldPos.y / _NoiseScale.y, 0) / _NoiseScale.z) - _Time.y * _NoiseSpeed;
                float noise = (cnoise(noise_position) + 1) / 2;
                noise *= map;
                noise = noise > _NoiseStep ? 1 : 0;

                float3 cut_position = (float3(i.worldPos.x / _CutScale.x, i.worldPos.y / _CutScale.y, 0) / _CutScale.z) - _Time.y * _CutSpeed;
                float cut = (cnoise(cut_position) + 1) / 2;
                cut = cut > _CutStep ? 1 : 0;

                float fire = noise - cut;
                float4 colored_fire = fire * color;

                return tex.a > 0.5 ? tex : colored_fire;
            }
            ENDCG
        }
    }
}
