using System;
using UnityEngine;

public class RenderMorpher : MonoBehaviour
{
    public enum RenderFormat {
        Morph,
        Wave,
        Noise,
        Offset
    }

    [SerializeField] private Material _renderMaterial;
    [SerializeField] private RenderTexture _inputTexture;
    [SerializeField] private ComputeShader _shader;
    [SerializeField] private RenderFormat _renderFormat;
    [Header("Shader settings")]
    [SerializeField] private Vector2 _windDirection;
    [SerializeField] private float _windSpeed;
    [SerializeField] private float _windDensity;
    [SerializeField] private float _windCutoff;
    [SerializeField] private float _wavePerturbationScale;
    [SerializeField] private float _wavePerturbationStrength;
    [Space(9)]
    [SerializeField] private float _horoffEdge;
    [SerializeField] private float _horoffScale;
    [SerializeField] private float _horoffSpeed;
    [Space(9)]
    [SerializeField] private float _vertoffEdge;
    [SerializeField] private float _vertoffScale;
    [SerializeField] private float _vertoffSpeed;

    private RenderTexture _resultTexture;

    private void RunShader()
    {
        if (!_resultTexture) {
            _resultTexture = new RenderTexture(100,80,24) {
                enableRandomWrite = true,
                filterMode = FilterMode.Point
            };
            _resultTexture.Create();
        }

        int kernelHandle = _shader.FindKernel("CSMain");

        _shader.SetInt("renderFormat", (int)_renderFormat);
        _shader.SetFloat("time", Time.time);
        _shader.SetVector("windDirection", _windDirection.normalized);
        _shader.SetFloat("windSpeed", _windSpeed);
        _shader.SetFloat("windDensity", _windDensity);
        _shader.SetFloat("windCutoff", _windCutoff);
        _shader.SetFloat("wavePerturbationScale", _wavePerturbationScale);
        _shader.SetFloat("wavePerturbationStrength", _wavePerturbationStrength);
        
        _shader.SetFloat("horoffEdge", _horoffEdge);
        _shader.SetFloat("horoffScale", _horoffScale);
        _shader.SetFloat("horoffSpeed", _horoffSpeed);

        _shader.SetFloat("vertoffEdge", _vertoffEdge);
        _shader.SetFloat("vertoffScale", _vertoffScale);
        _shader.SetFloat("vertoffSpeed", _vertoffSpeed);

        _shader.SetTexture(kernelHandle, "Input", _inputTexture);
        _shader.SetTexture(kernelHandle, "Result", _resultTexture);
        _shader.Dispatch(kernelHandle, 100/4, 80/4, 1);
    }

    private void Update()
    {
        RunShader();
        _renderMaterial.mainTexture = _resultTexture;
    }
}
