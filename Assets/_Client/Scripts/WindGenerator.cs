using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindGenerator : MonoBehaviour
{
    public enum OutputResult { Nothing, Strong, Medium, Weak }

    public static RenderTexture WindTexture { get; private set; }

    public RenderTexture StrongWindTexture {
        get {
            if (!_strongWindTexture) {
                _strongWindTexture = new RenderTexture(540, 360, 24) {
                    enableRandomWrite = true,
                    filterMode = FilterMode.Point
                };
                _strongWindTexture.Create();
            }
            return _strongWindTexture;
        }
    }
    public RenderTexture MediumWindTexture {
        get {
            if (!_mediumWindTexture) {
                _mediumWindTexture = new RenderTexture(540, 360, 24) {
                    enableRandomWrite = true,
                    filterMode = FilterMode.Point
                };
                _mediumWindTexture.Create();
            }
            return _mediumWindTexture;
        }
    }
    public RenderTexture WeakWindTexture {
        get {
            if (!_weakWindTexture) {
                _weakWindTexture = new RenderTexture(540, 360, 24) {
                    enableRandomWrite = true,
                    filterMode = FilterMode.Point
                };
                _weakWindTexture.Create();
            }
            return _weakWindTexture;
        }
    }

    [SerializeField] private ComputeShader _shader;
    [SerializeField] private Renderer _outputRenderer;
    [SerializeField] private OutputResult _outputResult;
    [Header("GlobalWave")]
    [SerializeField] private float _waveSpeed;
    [SerializeField] private float _waveLength;
    [Header("Distortion")]
    [SerializeField] private float _distortionStrength;
    [SerializeField] private Vector2 _distortionNoise1Scale;
    [SerializeField] private Vector2 _distortionNoise1Speed;
    [SerializeField] private Vector2 _distortionNoise2Scale;
    [SerializeField] private Vector2 _distortionNoise2Speed;
    [Header("Temp")]
    [SerializeField] private int _strongLevel;
    [SerializeField] private int _mediumLevel;
    [SerializeField] private int _weakLevel;

    private RenderTexture _strongWindTexture;
    private RenderTexture _mediumWindTexture;
    private RenderTexture _weakWindTexture;

    private void RunShader()
    {
        int kernelHandle = _shader.FindKernel("CSMain");


        _shader.SetFloat("time", Time.time);
        //GlobalWave
        _shader.SetFloat("waveSpeed", _waveSpeed);
        _shader.SetFloat("waveLength", _waveLength);
        //Distortion
        _shader.SetFloat("distortionStrength", _distortionStrength);
        _shader.SetVector("distortionNoise1Scale", _distortionNoise1Scale);
        _shader.SetVector("distortionNoise1Speed", _distortionNoise1Speed);
        _shader.SetVector("distortionNoise2Scale", _distortionNoise2Scale);
        _shader.SetVector("distortionNoise2Speed", _distortionNoise2Speed);
        //Temp
        _shader.SetInt("strongLevel", _strongLevel);
        _shader.SetInt("mediumLevel", _mediumLevel);
        _shader.SetInt("weakLevel", _weakLevel);


        _shader.SetTexture(kernelHandle, "StrongWind", StrongWindTexture);
        _shader.SetTexture(kernelHandle, "MediumWind", MediumWindTexture);
        _shader.SetTexture(kernelHandle, "WeakWind", WeakWindTexture);


        _shader.Dispatch(kernelHandle, 540 / 4, 360 / 4, 1);
    }

    private void Update()
    {
        RunShader();

        WindTexture = _strongWindTexture;
        Shader.SetGlobalTexture("_WindTexture", _strongWindTexture);
        Shader.SetGlobalTexture("_WeakWindTexture", _weakWindTexture);

        switch (_outputResult) {
            default:
            case OutputResult.Nothing:
                _outputRenderer.enabled = false;
                break;
            case OutputResult.Strong:
                _outputRenderer.enabled = true;
                _outputRenderer.sharedMaterial.mainTexture = StrongWindTexture;
                break;
            case OutputResult.Medium:
                _outputRenderer.enabled = true;
                _outputRenderer.sharedMaterial.mainTexture = MediumWindTexture;
                break;
            case OutputResult.Weak:
                _outputRenderer.enabled = true;
                _outputRenderer.sharedMaterial.mainTexture = WeakWindTexture;
                break;
        }
    }
}
