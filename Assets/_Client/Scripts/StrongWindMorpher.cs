using System;
using UnityEngine;

public class StrongWindMorpher : MonoBehaviour
{
    public enum RenderFormat
    {
        Morph,
        Wave,
        Noise,
        Offset
    }

    [SerializeField] private Material _renderMaterial;
    [SerializeField] private RenderTexture _inputTexture;
    [SerializeField] private ComputeShader _shader;
    [SerializeField] private RenderFormat _renderFormat;
    [SerializeField] [Range(2, 10)]
    private int _levels;
    [Header("Shader settings")]
    [Header("Strength")]
    [SerializeField] private float _strengthSpeed;
    [SerializeField] [Range(-1, 1)]
    private float _strengthCutoff;
    [Header("Wave")]
    [SerializeField] [Range(0, 1)]
    private float _waveInfluence;
    [SerializeField] private float _waveSpeed;
    [SerializeField] [Range(-1, 1)] 
    private float _waveCutoff;
    [SerializeField] private float _waveLength;
    [Header("Wave distortion")]
    [SerializeField] private float _distortion;
    [SerializeField] private float _distortion1Scale;
    [SerializeField] private float _distortion2Scale;
    [SerializeField] private float _distortionSpeed;


    private RenderTexture _resultTexture;

    private void RunShader()
    {
        if (!_resultTexture) {
            _resultTexture = new RenderTexture(540, 360, 24) {
                enableRandomWrite = true,
                filterMode = FilterMode.Point
            };
            _resultTexture.Create();
        }

        int kernelHandle = _shader.FindKernel("CSMain");

        _shader.SetInt("renderFormat", (int)_renderFormat);
        _shader.SetInt("levels", _levels);
        _shader.SetFloat("time", Time.time);

        _shader.SetFloat("strengthSpeed", _strengthSpeed);
        _shader.SetFloat("strengthCutoff", _strengthCutoff);

        _shader.SetFloat("waveInfluence", _waveInfluence);
        _shader.SetFloat("waveSpeed", _waveSpeed);
        _shader.SetFloat("waveCutoff", _waveCutoff);
        _shader.SetFloat("waveLength", _waveLength);

        _shader.SetFloat("distortion", _distortion);
        _shader.SetFloat("distortion1Scale", _distortion1Scale);
        _shader.SetFloat("distortion2Scale", _distortion2Scale);
        _shader.SetFloat("distortionSpeed", _distortionSpeed);

        _shader.SetTexture(kernelHandle, "Input", _inputTexture);
        _shader.SetTexture(kernelHandle, "Result", _resultTexture);
        _shader.Dispatch(kernelHandle, 540 / 2, 360 / 2, 1);
    }

    private void Update()
    {
        RunShader();
        _renderMaterial.mainTexture = _resultTexture;
    }
}
