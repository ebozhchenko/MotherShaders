using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static WindGenerator;


[RequireComponent(typeof(Renderer))]
public class WindShaderSupport : MonoBehaviour
{
    public int Width => (int)_mainSprite.rect.width;
    public int Height => (int)_mainSprite.rect.height;


    public RenderTexture Result {
        get {
            if (!_result) {
                _result = new RenderTexture(Width, Height, 24) {
                    enableRandomWrite = true,
                    filterMode = FilterMode.Point
                };
                _result.Create();
            }
            return _result;
        }
    }
    private RenderTexture _result;

    [SerializeField] private Sprite _mainSprite;
    [SerializeField] private Sprite _morphSprite;
    [SerializeField] private int _strength;
    [SerializeField] private ComputeShader _shader;

    private Renderer _renderer;


    private void RunShader()
    {
        int kernelHandle = _shader.FindKernel("CSMain");

        _shader.SetInt("strength", _strength);
        _shader.SetVector("position", transform.parent.position + new Vector3(WindGenerator.WindTexture.width / 2, WindGenerator.WindTexture.height / 2, 0));

        _shader.SetTexture(kernelHandle, "Main", ToTexture2D(_mainSprite));
        _shader.SetTexture(kernelHandle, "Morph", ToTexture2D(_morphSprite));
        _shader.SetTexture(kernelHandle, "Wind", WindGenerator.WindTexture);
        _shader.SetTexture(kernelHandle, "Result", Result);


        _shader.Dispatch(kernelHandle, Width / 2, Height / 2, 1);
    }

    private Texture2D ToTexture2D(RenderTexture rTex)
    {
        Texture2D tex = new Texture2D(512, 512, TextureFormat.RGB24, false);
        RenderTexture.active = rTex;
        tex.ReadPixels(new Rect(0, 0, rTex.width, rTex.height), 0, 0);
        tex.Apply();
        return tex;
    }

    private Texture2D ToTexture2D(Sprite sprite)
    {
        Texture2D tex = new Texture2D((int)sprite.textureRect.width, (int)sprite.textureRect.height) {
            filterMode = FilterMode.Point
        };
        Color[] pixels = sprite.texture.GetPixels((int)sprite.textureRect.x,
                                                (int)sprite.textureRect.y,
                                                (int)sprite.textureRect.width,
                                                (int)sprite.textureRect.height);
        tex.SetPixels(pixels);
        tex.Apply();
        return tex;
    }


    private void Start()
    {
        _renderer = GetComponent<Renderer>();
    }

    private void Update()
    {
        if (WindTexture != null) {
            RunShader();
            _renderer.sharedMaterial.mainTexture = Result;
        }
    }
}
