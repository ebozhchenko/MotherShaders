Shader "Unlit/StrongWindMorpher"
{
    Properties
    {
        _MainTex ("Main", 2D) = "white" {}
        _StrongWindTex ("Strong Wind", 2D) = "white" {}
    }
    SubShader
    {
        Tags {
            "RenderType" = "Transparent"
            "Queue" = "Transparent"
        }

        Blend SrcAlpha OneMinusSrcAlpha

        ZWrite off
        Cull off
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag


            #include "UnityCG.cginc"


            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
            };


            sampler2D _MainTex;
            sampler2D _StrongWindTex;
            float4 _MainTex_ST;
            float4 _StrongWindTex_ST;


            v2f vert (appdata v)
            {
                v2f o;

                float2 coord = UnityObjectToClipPos(v.vertex);
                float value = tex2D(_StrongWindTex, coord);

                o.vertex = UnityObjectToClipPos(v.vertex + float4(1, 0, 0, 0));
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);
                return col;
            }
            ENDCG
        }
    }
}
