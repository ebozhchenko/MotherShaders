Shader "Unlit/StrongWindShader"
{
    Properties
    {
        _MainTex("Main", 2D) = "white" {}
        _MorphTex("Morph", 2D) = "white" {}
        _MorphStrength("Strength", int) = 4
        _TexWidth("Texture Width", float) = 0
    }
        SubShader
    {
        Tags {
            "RenderType" = "Transparent"
            "Queue" = "Transparent"
        }

        Blend SrcAlpha OneMinusSrcAlpha

        ZWrite off
        Cull off
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag


            #include "UnityCG.cginc"


            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                float2 worldPos : TEXCOORD1;
            };


            sampler2D _MainTex;
            sampler2D _MorphTex;
            sampler2D _WindTexture;
            float4 _MainTex_ST;
            float4 _MorphTex_ST;
            float4 _WindTexture_ST;
            int _MorphStrength;
            float _TexWidth;


            v2f vert(appdata v)
            {
                v2f o;

                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);

                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float4 texPos = float4(i.worldPos.x / 540 + 0.5, i.worldPos.y / 360 + 0.5, 0, 0);
                float wind = tex2Dlod(_WindTexture, texPos);

                float4 morph = tex2Dlod(_MorphTex, float4(i.uv.xy, 0, 0));

                float offset = -(floor(wind * morph.r * _MorphStrength) / _TexWidth);


                fixed4 color = tex2D(_MainTex, i.uv + float2(offset, 0));
                return color;
            }
            ENDCG
        }
    }
}
